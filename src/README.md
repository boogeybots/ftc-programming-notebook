# Summary

- [Overview](./ch0_overview.md)
- [General Programming Decisions](./ch1_general_programming_decisions.md)
- [Software Team Goals](./ch2_software_team_goals.md)
- [Gigy 1.0.0-alpha.1](./ch3_gigy_1_0_0_alpha_1.md)
- [Gigy 1.0.0-alpha.2.pushbot](./ch4_gigy_1_0_0_alpha_2.md)
- [Gigy 1.0.0-alpha.3.spider](./ch5_gigy_1_0_0_alpha_3.md)
- [Gigy 1.0.0-beta.1+20181117.bucharest](./ch6_gigy_1_0_0_beta_1.md)
- [Gigy 1.0.0+20190105.iasi](./ch7_gigy_1_0_0.md)
- [Gigy 1.1.0+20190215](./ch8_gigy_1_1_0.md)
- [Lessons learned from the regionals](./ch9_regionals.md)
- [Gigy 3.0.0](./ch10_gigy_3_0_0.md)