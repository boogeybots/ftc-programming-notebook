---
prev: ch7_gigy_1_0_0
next: ch9_regionals
---

# Gigy 1.1.0+20190215.iasi
*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

## The learning never stops

Hardware-wise, the robot is pretty much the same as the last version, except for the team marker placement mechanism<sup>E5c</sup>. If the code worked before, why change it?  
Well, the code worked, but my brain was often close to shutting down when trying to read it. That's why for this version I reorganized a lot of code, and made other parts _smarter_.

## Project structure

For this version, I separated all the robot code in three packages: <span style="color: dodgerblue;">**autonomous**</span>, <span style="color: magenta">**teleop**</span> and <span style="color: orange">**utils**</span>.

## Utils

The <span style="color: orange">**utils**</span> package has three classes files: `Conversions.kt`, `Misc.kt` and `Robot.kt`.  
In `Conversions.kt` I wrote extension functions (hooray Kotlin) to the `Double` class to convert from inch to cm and back with ease:
```kotlin
fun Double.toInches(): Double {
    return this / 2.54
}

fun Double.toCentimeters(): Double {
    return this * 2.54
}
```

`Misc.kt` contains an extension function to `LinearOpMode` so that I can make the OpMode thread sleep safely (without crashes on Stop):
```kotlin
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode
import com.qualcomm.robotcore.util.ElapsedTime

val elapsedTime: ElapsedTime = ElapsedTime()

fun LinearOpMode.waitForSeconds(seconds: Double) {
    elapsedTime.reset()
    while (elapsedTime.seconds() < seconds) { }
}
```

The `Robot` class in `Robot.kt` stays at the base of any OpMode specific programming because it encapsulates all of the robot's hardware components and provides easy ways to access those. This is the equivalent of the hardware classes described in the previous editions. The only hardware changes are that the intake was replaced by the marker placement system and that the gyroscope resides in this class now:
```kotlin
class Robot(val opMode: OpMode) {
    // ...
    lateinit var markerServo: Servo
    lateinit var imu: BNO055IMU
    // ...
}
```

All the configuration variables and constants are stored in a `companion object` in this class:
```kotlin
companion object {
    const val DEFAULT_MOTOR_SPEED = 0.5
    const val MOVEMENT_MOTOR_TICK_COUNT = 1440
    const val WHEEL_DIAMETER = 4.0 // inches
    const val RATIO = 3.0
    const val WHEEL_CIRCUMFERENCE = Math.PI * 4.0 // inches

    val TFOD_MODEL_ASSET = "RoverRuckus.tflite"
    val LABEL_GOLD_MINERAL = "Gold Mineral"
    val LABEL_SILVER_MINERAL = "Silver Mineral"

    val VUFORIA_KEY = "VUFORIA_KEY_HERE"
}
```

Another cool this is that I'm using Kotlin's `vararg` to be able to modify multiple motors easily with those functions in the `Robot` class:
```kotlin
fun setMotorsMode(mode: DcMotor.RunMode, vararg motors: DcMotor) {
    for (motor in motors) {
        motor.mode = mode
    }
}

fun setMotorsPower(power: Double, vararg motors: DcMotor) {
    for (motor in motors) {
        motor.power = power
    }
}
``` 

I found out that my _very smart caching system_ is more annoying than helpful. I even coded it without any real, solid background on why I would do that. From this point of view, it's just a slightly more complicated setter. It conflicted with some of the movement logic from the autonomous period so I had to remove every _smart caching system_ and just use `.power` or `.position` (that should not even be a problem, because those were designed to be used; note to self: I should think more before I write)

## TeleOp

The `TeleOp` code is the almost same as before, but without the intake and the scoop. Besides that, the only change is that I don't use the _very smart caching system_ anymore for the hardware parts so I had to replace every `hardwarePartPower` with `hardwarePart.power`.

## Autonomous

The <span style="color: dodgerblue">**autonomous**</span> package has four files: `Crater.kt`, `Depot.kt`, `Movement.kt` and `Recognition.kt`. The first two contain the `LinearOpMode`s for the autonomous on each side of the lander and make heavy use on the functions defined in the other two files.  
I abstracted all the movement code into functions in the `Movement.kt` file. All those are extension functions for the `Robot` class for easy access and readability (so I'll be writing, for example, `robot.liftLock()` which makes a lot of sense when reading). That means I moved old code like `rotate`, `detachHook`, `dropDown` into this file.  

A nice improvement is that now I use the encoders that the engineers hooked up to the motors for precise movement. For that I wrote two functions: `Robot.moveByCentimeters` and `Robot.moveByInches`. The first one calls the second like so:
```kotlin
fun Robot.moveByCentimeters(centimeters: Double, power: Double = Robot.DEFAULT_MOTOR_SPEED) {
    moveByInches(centimeters.toInches(), power)
}
```

The `Robot.moveByInches` function calculates the target for the encoders from the wheel circumference and the rotation ratio between the wheels and the motors.

```kotlin

fun Robot.moveByInches(inches: Double, power: Double = Robot.DEFAULT_MOTOR_SPEED) {
    setMotorsMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER, leftMotor, rightMotor)

    val rotationsNeeded = inches / (WHEEL_CIRCUMFERENCE * RATIO)
    val drivingTarget = (rotationsNeeded * MOVEMENT_MOTOR_TICK_COUNT).toInt()

    leftMotor.targetPosition = drivingTarget
    rightMotor.targetPosition = drivingTarget

    leftMotor.power = power
    rightMotor.power = -power

    setMotorsMode(DcMotor.RunMode.RUN_TO_POSITION, leftMotor, rightMotor)

    // Wait for the movement to finish 
    while (leftMotor.isBusy || rightMotor.isBusy) { }

    setMotorsPower(0.0, leftMotor, rightMotor)

    setMotorsMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER, leftMotor, rightMotor)
}
```

Another change is the movement on angle, which is now in the functions `Robot.moveByInchesOnAngle` and `Robot.moveByCentimetersOnAngle` which combine concepts from the the old movement on angle and the new encoder-driven movement.

On the mineral recognition side, the code is mostly the same, just with a little, but significant change (at least for me). I found myself often changing code in the wrong branch of movement, because I wasn't reading the gold position correctly from the integers. Now, the recognition function, which is `Robot.recognizeGold` inside `Recognition.kt` returns a `GoldPos` enum:

```kotlin
enum class GoldPos {
    LEFT, MIDDLE, RIGHT
}

fun Robot.recognizeGold(): GoldPos {
    // ...

    return when (k) {
        1 -> GoldPos.MIDDLE
        2 -> GoldPos.LEFT
        else -> GoldPos.RIGHT
    }
}
```

Moving on to the specific implementation for each of the starting points, the movement path is more or less the same as in the last version, but is easier to write and read, thanks to the new movement methods. That means it's mostly a chain of `robot.moveByCentimetersOnAngle` and `robot.rotate`, besides the initial
```kotlin
robot.liftLock()
robot.dropDown()
robot.pushLander()
robot.detachHook()
robot.rotate(-170.0, 0.3)
```
and the ocasional waits and the team marker placement.

Thanks to these improvements, a complete autonomous program would look pretty much like this:
```kotlin
// In initialization I set up the hardware and 
// vuforia for mineral recognition
initialization()
waitForStart()

robot.liftLock()
robot.dropDown()
robot.pushLander()
robot.detachHook()
robot.rotate(-180.0)

robot.moveByCentimetersOnAngle(DIST_TO_MIN_REC_POS, angle = 0.0, power = 0.3)

val goldPos = robot.recognizeGold()

when (goldPos) {
    GoldPos.LEFT -> {
        // How to move when the gold mineral is in the left
    }
    GoldPos.MIDDLE -> {
        // How to move when the gold mineral is in the middle
    }
    GoldPos.RIGHT -> {
        // How to move when the gold mineral is in the right
    }
}
```



___
0. https://semver.org/