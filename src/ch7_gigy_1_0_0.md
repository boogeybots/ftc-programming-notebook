---
prev: ch6_gigy_1_0_0_beta_1
next: ch8_gigy_1_1_0
---

# Gigy 1.0.0+20190105.iasi
*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

## :tada: First major version :tada:

This is the first version that has complete autonomous and more functionality besides just moving as a TeleOp.

## <span style="color: magenta">TeleOp</span>

This version still uses the `RoverRuckusHardware` class to separate hardware from logic. It differs from the last version by adding more components to the robot: 
![hardware class](./img/1.0.0/hardware_class.png)

It still uses caching for the motors and it applies it to the servos also. Here is an example for the scoop servo:

![servo caching](img/1.0.0/servo_caching.png)

The new components are also set up from the provided hardware map:

![hardware init](img/1.0.0/hardware_init.png)

The `TeleOp` `OpMode` now has to initialize a bunch of new fields, like so:

![opmode fields](img/1.0.0/opmode_fields.png)

The `init` function still consists of only the hardware class initialization:

![opmode init](./img/1.0.0/opmode_init.png)

The movement differs from the last version by enabling speed tweaking. Notice that the motors' powers are clipped using the `currentSpeed` variable:
```kotlin
hardware.leftMotorPower = Range.clip(throttle - brake - horizontalMovement, -currentSpeed, currentSpeed)
hardware.rightMotorPower = Range.clip(throttle - brake + horizontalMovement, -currentSpeed, currentSpeed)
///======================

///=== SPEED SETTINGS ===
if (gamepad1.dpad_up) {
    currentSpeed = Range.clip(currentSpeed + 0.01, minSpeed, maxSpeed)
}
if (gamepad1.dpad_down) {
    currentSpeed = Range.clip(currentSpeed - 0.01, minSpeed, maxSpeed)
}
///=======================
```

I had to introduce movement for all the new parts - lock, scoop, hook and intake:
```kotlin
 ///=== LOCK ===

lockOffset += ((if (gamepad1.y) lockSpeed else 0.0) - (if (gamepad1.a) lockSpeed else 0.0))
lockOffset = Range.clip(lockOffset, -0.5, 0.0)
hardware.lockServoPos = Range.clip(0.5 + lockOffset, 0.0, 1.0)

///=== ARM ===

val armMovement: Double = -gamepad2.left_stick_y.toDouble()
hardware.leftArmPower = Range.clip(armMovement, -0.6, 0.6)
hardware.rightArmPower = Range.clip(armMovement, -0.6, 0.6)
// We found that 0.6 is a good power for the arm motors power.
// This way the arm moves neither too slow nor too fast.

///=== SCOOP ===

scoopOffset += (gamepad2.left_trigger.toDouble() - gamepad2.right_trigger.toDouble())
scoopOffset = Range.clip(scoopOffset, -0.05, 0.5)
hardware.scoopServoPos = Range.clip(0.5 + scoopOffset, 0.0, 1.0)
// I clip the offset value with the min = -0.05 because I need the
// scoop to be able to perfectly touch the ground
// and for that it need to have a position of 0.45 (0.5 + (-0.05))

///=== HOOK ===

hookOffset += ((if (gamepad2.left_bumper) hookSpeed else 0.0) - (if (gamepad2.right_bumper) hookSpeed else 0.0))
hookOffset = Range.clip(hookOffset, 0.0, 0.5)
hardware.hookServoPos = Range.clip(0.5 + hookOffset, 0.5, 1.0)

///=== INTAKE ==

leftIntakeOffset += (-gamepad2.right_stick_y) * intakeSpeed
rightIntakeOffset += gamepad2.right_stick_y * intakeSpeed
leftIntakeOffset = Range.clip(leftIntakeOffset, -0.5, 0.0)
rightIntakeOffset = Range.clip(rightIntakeOffset, 0.0, 0.5)

hardware.leftIntakeServoPos = Range.clip(0.5 + leftIntakeOffset, 0.0, 0.38)
hardware.rightIntakeServoPos = Range.clip(0.5 + rightIntakeOffset, 0.62, 1.0)
```


## <span style="color: dodgerblue">Autonomous</span>

As I said in the beginning of this chapter, the robot had full autonomous. To achieve that, I had to improve the precision a bit so I started using the gyroscope built into the Rev Expansion Hub:
```kotlin
class AutoCrater : LinearOpMode() {
    // ...
    lateinit var imu: BNO055IMU

    private lateinit var angles: Orientation
    private lateinit var gravity: Acceleration

    var lastAngles: Orientation = Orientation()
    var globalAngle: Double = 0.0

    override fun runOpMode() {
        // ...
        val imuParams = BNO055IMU.Parameters()
        imuParams.angleUnit = BNO055IMU.AngleUnit.DEGREES
        imuParams.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC
        imuParams.calibrationDataFile = "BNO055IMUCalibration.json"
        imuParams.loggingEnabled = true
        imuParams.loggingTag = "IMU"
        imuParams.accelerationIntegrationAlgorithm = JustLoggingAccelerationIntegrator()

        imu = hardwareMap.get(BNO055IMU::class.java, "imu")
        imu.initialize(imuParams)

        imu.startAccelerationIntegration(Position(), Velocity(), 1000)
        // ...
    }
}
```

When the game starts, the robot has to come down from the lander. That is achieved by a chain of suggestive functions:
```kotlin
liftLock()
dropDown()
pushLander()
moveLock()
goTowardsLanderToLiftHook()
detachHook()
```
Each of these modifies some motor powers or servo positions for a brief period of time to achieve the desired kind of movement. The code for each one can be found on GitHub<sup>1</sup>.  

Using a bunch of movement functions such as `goForwardOnAngle` or `rotate` the robot manages to complete all the tasks in autonomous by (more or less) following a predefined path. The path, in code, is just a big pile of those functions and a few others. For example, here is what the robot does when the gold mineral is in the center:
```kotlin
goForwardOnAngle(time = 0.7, power = 0.45, angle = 180.0)
waitForSeconds(0.7)
goForwardOnAngle(time = 0.7, power = -0.45, angle = 180.0)
rotate(degrees = -90.0, power = 0.34)
goForwardOnAngle(time = 1.6, power = -0.45, angle = 90.0)
waitForSeconds(0.2)
rotate(degrees = 45.0, power = 0.34)
goForwardOnAngle(time = 1.35, power = -0.45, angle = 143.0)
dropTeamMarker()
waitForSeconds(0.5)
goForwardOnAngle(time = 0.2, power = 0.8, angle = 142.0)
goForwardOnAngle(time = 1.1, power = 0.5, angle = 142.0)
goForwardOnAngle(time = 1.0, power = 0.5, angle = 147.0)
letDownArm()
```

Because the code is not very well organized I think it isn't worth showing every line here in the notebook, as 500 lines / autonomous (crater + depot = 1000 lines) means too much ugly code, that's both hard to read and to explain nicely. I'll come back to some parts in the next chapter, as I refactored the majority of the codebase.  

What's worth showing here though, is represented by the `rotate` and `goForwardOnAngle`.

The `rotate` function needs two helpers: `resetAngle` and `getAngle`.
```kotlin
private fun resetAngle() {
    lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES)

    globalAngle = 0.0
}

fun getAngle(): Double {
    val angles: Orientation = 
        imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES)

    var deltaAngle = angles.firstAngle - lastAngles.firstAngle

    if (deltaAngle < -180) {
        deltaAngle += 360
    } else if (deltaAngle > 180) {
        deltaAngle -= 360
    }

    globalAngle += deltaAngle

    lastAngles = angles

    return globalAngle
}
```

`genAngle` returns the `globalAngle`, meaning the overall Z-axis orientation of the robot, while `resetAngle` resets this `globalAngle` to `0.0`, meaning the orientation is computed from the position of the robot from the last call to `resetAngle`.  
Here is the full `rotate` function:

```kotlin
fun rotate(degrees: Double, power: Double) {
    var leftPower: Double
    var rightPower: Double

    when {
        degrees < 0 -> {
            leftPower = power
            rightPower = -power
        }
        degrees > 0 -> {
            leftPower = -power
            rightPower = power
        }
        else -> return
    }

    hardware.leftMotorPower = leftPower
    hardware.rightMotorPower = rightPower

    if (degrees - getAngle() < 0) {
        while (opModeIsActive() && getAngle() == 0.0) { telemetry.update() }
        while (opModeIsActive() && getAngle() > degrees) { telemetry.update() }
    } else {
        while (opModeIsActive() && getAngle() < degrees) { telemetry.update() }
    }

    resetMovementMotors()

    // wait for rotation to stop.
    sleep(1000)

    // reset angle tracking on new heading.
    resetAngle()
}
```

The `goForwardOnAngle` function gets the current orientation of the robot and if the angle from the gyroscope is outside of some threshold values relative to the specified angle, the robot turs inward, to get closer to the specified angle.
```kotlin
fun goForwardOnAngle(time: Double, power: Double, angle: Double) {
    elapsedTime.reset()
    val leftLimiter: (Double) -> Boolean
    val rightLimiter: (Double) -> Boolean
    val higherPower = power + 0.25
    val lowerPower = power - 0.25

    // The tricky stuff is when trying to follow a path on 180 degrees from the initial position
    // because the gyroscope returns the angle like so:
    // 179.7 179.8 179.9 ??? -179.9 -179.8 -179.7 ...
    // So you got to keep that in mind and check for two conditions in
    // the limiter lambdas to avoid messing up the turning

    if (angle == 180.0) {
        leftLimiter = { rotation -> rotation < 0 && rotation > -177.5 }
        rightLimiter = { rotation -> rotation > 0 && rotation < 177.5 }
    } else {
        leftLimiter = { rotation -> rotation > angle + 2.5 }
        rightLimiter = { rotation -> rotation < angle - 2.5 }
    }
    while (elapsedTime.seconds() < time && opModeIsActive()) {
        val rotation = imu
                .getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES)
                .firstAngle
                .toDouble()
        when {
            leftLimiter(rotation) -> {
                hardware.leftMotorPower = higherPower
                hardware.rightMotorPower = lowerPower
            }
            rightLimiter(rotation) -> {
                hardware.leftMotorPower = lowerPower
                hardware.rightMotorPower = higherPower
            }
            else -> {
                hardware.leftMotorPower = power
                hardware.rightMotorPower = power
            }
        }
    }
    resetMovementMotors()
}
```

___
0. https://semver.org/
1. https://github.com/BoogeyBots/RoverRuckus/blob/c2f08a43527fc54a7e69eaebfd2ee3f68e38f4bf/TeamCode/src/main/java/org/firstinspires/ftc/teamcode/roverruckus/AutoCrater.kt