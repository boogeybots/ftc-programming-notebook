---
next: ch2_software_team_goals
---

# General Programming Decisions

## Programming Environment
I chose Android Studio as the programming environment because I knew my way around (a bit). I don't feel confortable with blocks programming. I consider text to be superior and more flexible. It also sounds better (in my opinion) for a possible future employer to hear "I know to write Kotlin" than "I know how to program using blocks", because a programming language like Kotlin or Java has multiple use cases and is quite common in the industry.

TL;DR I like text more than blocks programming.

I tried a few times to develop Android apps, but it was mostly an exercise, because I didn't have any serious project idea in mind. I also installed a custom ROM on my Android phone so I also interacted with the Android Debug Bridge (adb).

## Programming Language
**The Game Manual, part 1**<sup>1</sup> in chapter **8.2.1 Robot Technology Definitions**, on page 25, says that Java is defined as: 
>*Java* – The recommended programming language for the *Robot Controller*

However, I decided to go with Kotlin. Why? Because Kotlin:
- feels more **modern**
- is **easier** to **read** and **write**
- is **interoperable** (I can use exsting for the JVM or Android)
- **tool friendly** (to use Kotlin in Android Studio, I just create a *.kt* file and the IDE configures everything for me)
- classes have **properties** (coming from C#, at this points Kotlin starts to feel like home)
- is an **official Android programming language** with **first-class support**<sup>2</sup>

For a more detailed and technical comparison, visit the **Comparison to Java Programming Language** page at *kotlinlang.org*<sup>3</sup>
A document that was very useful in the beginning, when a I had to translate a lot of code from Java to Kotlin, was Fabio Santana's **From Java to Kotlin**<sup>4</sup>
 

## Version Control
A world without git is a world I don't want to live in. I mean, yes, of course other VCS's exist but git... What would GitHub be without git? Just another Hub? I hope you don't think of *that* hub!  
I use **git**<sup>5</sup> and **GitHub**<sup>6</sup> to avoid having 50 to 100 ftc-final-almostfinal-done-movementchange-final-lastchange-... folders on my drives and to enable other teams to look through our code and improve theirs.  
All our code is public on GitHub, in the **BoogeyBots/robot_code** repository<sup>7</sup>, released under the **GNU General Public License version 3.0 (GNU GPLv3)**<sup>8</sup>.

By the way, here are some must-read git koans written by Steve Losh: http://stevelosh.com/blog/2013/04/git-koans/

## What about the other programmers? You only talk about yourself...

Yes, I only talk about myself - because I am pretty much *the only programmer* here. Roxana also wanted to become one but actually never managed to get to spend enough time around the robot coding (because hands-on learning is the best in this case) because she got caught up with other things. Came for the code, stayed for the engineering notebook.  

At the time of writing, I still am the sole programmer on this team but there were more attempts, beside from Roxana. When we recruited new members, I had a group of around 8 people that wanted to become programmers. Taught them git and GitHub to enable them to collaborate with us on the code, sent them a bunch of Kotlin and FTC programming resources, and never heard from them again. It's true I didn't forgot to poke them from time to time but I didn't have enough time to code the robot and to force others to learn to code, especially since it's our first year and it took a while to stabilize our routines and tasks and overall organization as a team.  
So, I want to say **I'm sorry I haven't stressed you enough with programming to you guys that wanted to join me** and also **you should have stressed me also because its a known fact that my memory has a few bugs** (it randomly uses `delete` from time to time...).  

And that's the story of how I become the only programmer at BoogeyBots. It has its pros, like being able to work in your own style and at your preffered pace (as long as it's fast enough so the Engineers and Drivers don't start whipping you), but I think someone to share the burden and the love for what I do would have been fantastic. 

## Programming Notebook Section Style

For the first versions - **1.0.0-alpha.1** to **1.0.0-alpha.3.servos** - this section of the notebook will be comprised more of stories and descriptions of *what* and *why*, with little *how* sprinkled in plain text, since those were training versions and not actual competition robots. From **1.0.0-beta.1+20181117.bucharest** I'll start showing code and explaining in more details concrete decisions and implementations.  
Each version has a superscript attached that points to the related section in the Engineering section of the Notebook. (e.g. version<sup style="color: orange;">E1</sup>)

___
1. https://www.firstinspires.org/sites/default/files/uploads/resource_library/ftc/game-manual-part-1.pdf
2. https://developer.android.com/kotlin/
3. https://kotlinlang.org/docs/reference/comparison-to-java.html
4. https://fabiomsr.github.io/from-java-to-kotlin/
5. https://git-scm.com/
6. https://github.com/
7. https://github.com/BoogeyBots/robot_code
8. https://www.gnu.org/licenses/gpl-3.0.en.html

