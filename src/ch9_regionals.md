---
prev: ch8_gigy_1_1_0
next: ch10_gigy_3_0_0
---

# What we learned from the Regionals

The Iași regionals were a good opportunity for me to see how other people think in code. I got to talk to a bunch of friendly programmers and even look at some of their code.  

While trying to help others solve some of their problems, I spotted some of my own.

## Stupid power-setting loops  
   We were trying to fix an OpMode that crashed when Stop was pressed and we were replacing all the `sleep`s with:
   ```kotlin
   while (elapsedTime.seconds < time && opModeIsActive()) { }
   ```
   What left me with my jaw dropped was the fact that the loop was empty. I had a few of those empty loops myself but the idea was that the loop was empty even when they needed motors running during that time.  
   In my code, such a thing looked like:
   ```kotlin
   while (elapsedTime.seconds < time && opModeIsActive()) {
       setMotorsPower(power, motor1, motor2)
   }
   ```
   Now, it's easy to see why that's just stupid. The powers should be set before the loop starts, that way we're saving a lot of useless function calls:
   ```kotlin
   setMotorsPower(power, motor1, motor2)
   while (elapsedTime.seconds < time && opModeIsActive()) { }
   ```
## Fail-**un**safe recognition  
   When the robot would look at the minerals to try to spot the gold one, if for some reason it could not identify any minerals, it would just wait until it saw something. Even though we did not have any problems at the regionals, I should modify the code so that the robot only allocates a bit of time to look for each mineral, and if it sees nothing, it should continue doing all the other stuff.
## Mixing encoders with gyroscopes  
   When I thought "I'm going to make the robot drive straight for a given distance calculated with encoders by reading the angles from the gyroscope and using them to correct the movement" it sounded like the best possible solution for precise movement.  
   
   It turns out it isn't (and it's not even that hard to figure out why). When I want the robot to move a specific distance, I compute a number of ticks that I feed to the motor. If the robot goes out of the threshold angle and tries to come back in (i.e. tries to follow a straight line), it supplies more power to one of the wheels to turn a little. That means that one of the wheels performs more ticks than the other for a short period of time. So at the end of the correction one wheel completed X ticks and the other Y, where X > Y. Because of that, when one wheel finished rotation, if you don't have the right condition in a `while` loop, the other motor will still run because it didn't reach the target position yet. This means that at the end the robot rotates in place, messing up all the following movement. Also, because of the corrections, even if the robot would not turn at the end, it does not perform as wanted, because the distance I supply to the movement function is mean to represent forward movement, and the robot loses some of this distance by rotating left-right.  

   Theoretically, you could mix encoders with gyroscopes. But it's too complicated to be worth it. If I need to use direction-driven movement-by-distance I have to find an alternative to using the gyroscope or to drop the "-by-distance" part. 