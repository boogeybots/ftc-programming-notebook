---
prev: ch2_software_team_goals
next: ch4_gigy_1_0_0_alpha_2
---

# Gigy 1.0.0-alpha.1

*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

## Gigy 1.0.0-alpha.1.tetrix

Being the very first robot build built by BoogeyBots (and actually only a drivetrain), the code was very simple.
All it did was make the robots move while being controlled. The code was adapted from the Pushbot TeleOp Tank movement example<sup>1</sup>, pretty much all the same, but without the arm hardware and logic. It was in this moment that I found out about the Java to Kotlin conversion integrated in Android Studio, a feature that I absolutely love and used ever since.
At the moment, I had little knowledge of Java and even less of Kotlin, but it helped being used with other C-family languages, like C++ and C#.
The learning curve wasn't too big; I'm used to learning new frameworks, so concepts like `OpMode`, `hardwareMap`, and `telemetry` came easy to me.

## Gigy 1.0.0-alpha.1.rev

Code-wise, it's the same as **1.0.0-alpha.1.tetrix**.

___
0. https://semver.org/
1. https://github.com/ftctechnh/ftc_app/blob/master/FtcRobotController/src/main/java/org/firstinspires/ftc/robotcontroller/external/samples/PushbotTeleopTank_Iterative.java