---
prev: ch3_gigy_1_0_0_alpha_1
next: ch5_gigy_1_0_0_alpha_3
---

# Gigy 1.0.0-alpha.2.pushbot

*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

This version of Gigy was pretty important because we learned how configure the phones better and I learned how servos work and how to map more than 2 basic controls to gamepads (you read that right, gamepad*s*, like in **two gamepads**).
Our robot was a slightly modified version of the official PushBot<sup>1</sup>, with 2 servos that were supposed to lock the arm so the robot could grab the lander and lift itself and just stay there (guess what, it didn't work).  

One of the things that I consider pretty significant for the programming part of this robot was the separation of the hardware representation from the TeleOp logic<sup>2,3</sup> with the Hardware class (all code is taken from the example, nothing inovative here).  

Another one is a caching system for a motor's power. A programmer from Quantum Robotics (RO077) showed me this in his code. I couldn't exactly remember their implementation and motivation for this one but it seemed logical to me - don't tell the motors SET THE POWER TO X SET THE POWER TO X SET THE POWER TO X every "frame", tell them "chill dude, so let's see, is your power the same as the new power? yes? then don't try to set anything. no? then set the new power".  
Here is the code:  
```kotlin
var leftMotorPower: Double = 0.0
    // Custom setters for caching system
    set(value) {
        // Only set the power of the motor if the last "frame's" power is
        // different from the current one
        if (value != field) {
            field = value
            leftMotor.power = field
        }
    }
```
<!-- ![Power caching code](img/1.0.0-alpha.2.pushbot/power_cache.png) -->

That's pretty much it for this version, I don't remember doing other notable stuff. To remind you, the first significant version is [1.0.0-beta.1+20181117.bucharest](#gigy-100-beta120181117bucharest). There I will explain more of the code and the thinking behind it, because I will have a program not written for tests or learning, but one that really does stuff.
 
___
0. https://semver.org
1. https://www.firstinspires.org/sites/default/files/uploads/resource_library/ftc/2016-2017-season/pushbot-build-guide.pdf
2. https://github.com/ftctechnh/ftc_app/blob/master/FtcRobotController/src/main/java/org/firstinspires/ftc/robotcontroller/external/samples/PushbotTeleopTank_Iterative.java
3. https://github.com/ftctechnh/ftc_app/blob/master/FtcRobotController/src/main/java/org/firstinspires/ftc/robotcontroller/external/samples/HardwarePushbot.java
