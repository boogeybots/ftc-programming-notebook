---
prev: ch4_gigy_1_0_0_alpha_2
next: ch6_gigy_1_0_0_beta_1
---

# Gigy 1.0.0-alpha.3.spider
*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

This design was supposed to lift or drop the whole robot with 4 moving legs.  
At the time of writing, I couldn't find any of the code I wrote for this version. This is probably because the whole concept didn't work. The servos that were supposed move the whole chasis vertically weren't powerful enough and the whole robot ended slowly going down.
From what I recall, we read the servo positions (0 - leftmost, 0.5 - center, 1 - rightmost) from the servo programmer. Knowing these positions we restricted the legs vertical movement until a certain value (experimentally determined).

___
0. https://semver.org