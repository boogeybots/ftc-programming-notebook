# Programming overview

Being our first season, we experimented with a lot of stuff regarding programming.  

As you'll read in the first chapters, we use Git and GitHub for source control and Kotlin as the programming language for the robot. More programming decisions are described in the General Programming Decisions chapter.  

Inexperienced as we ~~are~~ were, we still got a full autonomous working. But that meant a lot of iterations and work. I wrote our "journey", so to say, from our first robot to our current one, emphasizing on what we've learned and how we got things to work. You can follow our progress by reading all the chapters that are associated with a robot version.  

Significant progress was done in:
- 1.0.0-beta.1, in which we implemented mineral recognition
- 1.0.0, in which we managed to get a full autonomous program to work
- 1.1.0, in which we started using encoders
- 3.0.0, in which we added navigation target scanning

## TeleOp

Drivers play the most important role during matches, so it is natural that we wanted to make their job as easy and pleasant for them as possible to achieve the best results.

Driver-focused enhancements:
- split controls between two controllers
- robot speed tweaking
- drivers set the controls according to their personal preferences
- some parts of the robot run on encoders for precise movement on button presses

Control mappings:

![Controller 1 mappings](./img/overview/gamepad1.png)
![Controller 2 mappings](./img/overview/gamepad2.png)

## Autonomous

Our team implemented a single autonomous program for this season, designed to maximize scoring in any given scenario. The autonomous paths followed by our robot, Gigy, are designed to have a very low chance of colliding with other robots' paths this meaning efficient scoring when allied with robots that present different strategies.  

The cool things about Gigy's autonomous period are:
- Movement
- Rotation
- Gold mineral recognition
- Navigation target scanning

Key technologies used:
- Motor encoders
- Gyroscope
- Vuforia
- Tensorflow

### Autonomous diagram

![](./img/overview/auto_diagram.png)