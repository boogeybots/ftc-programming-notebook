---
prev: ch9_regionals
---

# Gigy 3.0.0
*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

> \- What about 2.0?  
> \- It's forbidden to talk about 2.0...

This is our first serious robot. It weights double the size of the old Gigy and does twice the amout of things old Gigy did. It combines what we learned through building and programming the 1.x version and the 2.x version. (Programming-wise, the 2.x version was mostly testing code for individual system, until we figured out the fact that it was impossible to work.)

## Asynchronous Programming

Because we're limited on 30 seconds in the autonomous period, the robot needs to be quick. If it were to all the moves one after another, it would easily exceed the time limit. Just the lifting system takes 8 seconds to do a complete move. That means we need to find a way to execute multiple tasks simultaneously.  

The solution I found was using [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines/basics.html).  

The basics are pretty simple: to run some code in parallel to the main thread, launch a coroutine by writing your code inside `GlobalScope.launch {}`. This function returns a `Job` that acts as a reference to the created coroutine. To have control over the launched coroutines, I store them inside a `MutableList`, and when one of them ends, it is automatically removed from the list (the code inside `job.invokeOnCompletion`):

```kotlin
var jobs: MutableList<Job> = mutableListOf<Job>()

fun runParallel(foo: () -> Unit) {
    val job = GlobalScope.launch {
        foo()
    }
    job.invokeOnCompletion {
        jobs.remove(job)
    }
    jobs.add(job)
}
```

As you can see, the `runParallel` function accepts a lambda. Kotlin's syntax enables us to quickly integrate existing code with this function:
```kotlin
runParallel {
    robot.moveLifting(up = false)
}
```

To ensure all the jobs are completed before the robot stops, I need to iterate all the jobs in the list and wait for them to end, by suspending the caller coroutine until the job is complete with `join()`.

```kotlin
suspend fun finishAllJobs() {
    for (job in jobs) {
        job.join()
    }
}
```

To use all this stuff from inside the `LinearOpMode`, all the opMode code is wrapped inside `runBlocking`:
```kotlin
override fun runOpMode() = runBlocking {
    // ...
    finishAllJobs()
}
```

## Utils

For this version I wanted to have easier access to hardware such as DC motors or servos, and that's why I store them in two HashMaps:  

```kotlin
lateinit var motors: HashMap<Motors, DcMotor>
lateinit var servos: HashMap<Servos, Servo>
``` 

`Motors` and `Servos` are two enums that work as keys to easily access the values from the containers:  

```kotlin
enum class Motors {
    LF,
    RF,
    LB,
    RB,
    Lift,
    IntakeRotation,
    IntakeExtension,
    Sweeper
}

enum class Servos {
    Phone,
    SweeperLock
}
```

The motors and servos are stored in the maps in the initialization function of the `Robot` class:  

```kotlin
fun init() {
    motors = hashMapOf(
        Motors.LF to hardwareMap.get(DcMotor::class.java, "lf"),
        Motors.RF to hardwareMap.get(DcMotor::class.java, "rf"),
        Motors.LB to hardwareMap.get(DcMotor::class.java, "lb"),
        Motors.RB to hardwareMap.get(DcMotor::class.java, "rb"),
        Motors.Lift to hardwareMap.get(DcMotor::class.java, "lift"),
        Motors.IntakeRotation to hardwareMap.get(DcMotor::class.java, "i_r"),
        Motors.IntakeExtension to hardwareMap.get(DcMotor::class.java, "i_e"),
        Motors.Sweeper to hardwareMap.get(DcMotor::class.java, "sweeper")
    )

    motors[Motors.IntakeRotation]?.mode = DcMotor.RunMode.RUN_WITHOUT_ENCODER

    motors[Motors.Lift]?.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
    motors[Motors.IntakeRotation]?.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
    motors[Motors.IntakeExtension]?.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE


    motors[Motors.LF]?.direction = DcMotorSimple.Direction.REVERSE
    motors[Motors.RF]?.direction = DcMotorSimple.Direction.FORWARD
    motors[Motors.LB]?.direction = DcMotorSimple.Direction.REVERSE
    motors[Motors.RB]?.direction = DcMotorSimple.Direction.FORWARD

    servos = hashMapOf(
        Servos.Phone to hardwareMap.get(Servo::class.java, "sph"),
        Servos.SweeperLock to hardwareMap.get(Servo::class.java, "sl")
    )

    // Gyroscope initialization (same as before)
}
```

Because we're storing motors inside a HashMap, Kotlin's safety rules enforce us to use the `?` (question mark) operator _a lot_. Using containers is great for storing and iterating, but this method means long and ugly syntax: `robot.motors[Motors.LF]?.power = 0.0` . Another example, if I wanted to check if a motor is busy running to a specific position I would need to write: `motors[motor]?.mode == DcMotor.RunMode.RUN_TO_POSITION && motors[motor]?.isBusy!!` .  

That's why I created a bunch of helper functions to set the power, mode, target position, and check if a motor is busy:  

```kotlin
fun setMotorMode(motor: Motors, mode: DcMotor.RunMode) {
    this.motors[motor]?.mode = mode
}

fun setMotorsMode(mode: DcMotor.RunMode, vararg motors: Motors) {
    for (motor in motors) {
        setMotorMode(motor, mode)
    }
}

fun setMotorPower(motor: Motors, power: Double) {
    this.motors[motor]?.power = power
}

fun setMotorsPower(power: Double, vararg motors: Motors) {
    for (motor in motors) {
        setMotorPower(motor, power)
    }
}

fun setMotorTargetPos(motor: Motors, position: Double) {
    this.motors[motor]?.targetPosition = position.toInt()
}

fun setMotorsTargetPos(position: Double, vararg motors: Motors) {
    for (motor in motors) {
        setMotorTargetPos(motor, position)
    }
}

fun isMotorBusy(motor: Motors): Boolean {
    return motors[motor]?.mode == DcMotor.RunMode.RUN_TO_POSITION && motors[motor]?.isBusy!!
}

fun areAllMotorsBusy(vararg motors: Motors): Boolean {
    return motors.all { m -> isMotorBusy(m) }
}

fun areMotorsBusy(vararg motors: Motors): Boolean {
    return motors.any { m -> isMotorBusy(m) }
}
```

## TeleOp

### Driving

Because the chasis has wheels at a 45° angle two of the motors needed to run in reverse to be able to make the robot go forward on positive motor powers, those being the motors on the left.  

![motors](./img/3.0.0/motors_reverse.png)

### Control mapping

#### Gamepad 1
![gamepad1](./img/3.0.0/gamepad1.png)

#### Gamepad 2
![gamepad2](./img/3.0.0/gamepad2.png)

### Intake system

We tried two strategies regarding the movement of the **intake system**. 

The first one was using encoders to extend and rotate the arm to fixed positions. Is supposed having two buttons, one for extension and one for rotation.  

The reason we thought about this was that it would ease the work of the drivers as they would only need to push two buttons to take the minerals from the crater to the lander.  

This method was unsuccessful because there were cases in which the drivers needed to manually move the arm. We ended up making the extension and the rotation of the arm manual.

```kotlin
// INTAKE ARM ROTATION 
val intakeArmPower = gamepad2.right_stick_y.toDouble()
robot.motors[IntakeRotation]?.power = Range.clip(intakeArmPower, -0.65, 0.65)

// INTAKE ARM EXTENSION 
val armPower = -gamepad2.left_stick_y.toDouble()
robot.motors[IntakeExtension]?.power = armPower.clip(-0.8, 0.8)
```

### Lifting

During the driving period, the lifting system is extended to optimize the time drivers spend on latching on the lander. The lifting movement is done using encoders.  

```kotlin
//===============
//=== LIFTING ===
//===============

val GEAR_RATIO = 3.7 // The gear ratio for 5202 Series Yellow Jacket Planetary Gear Motor
val CPR = 28 // Encoder Countable Events Per Revolution (Encoder Shaft)
val ROTATIONS = 70 // Rotations (computed by trial and error)

//...

if (gamepad1.x && !robot.isMotorBusy(Lift) && isLiftUp) {
    robot.setMotorMode(Lift, DcMotor.RunMode.STOP_AND_RESET_ENCODER)

    robot.motors[Lift]?.targetPosition = (CPR * GEAR_RATIO * -ROTATIONS).toInt()

    robot.motors[Lift]?.power = -0.99
    robot.motors[Lift]?.mode = DcMotor.RunMode.RUN_TO_POSITION

    isLiftUp = !isLiftUp
}
if (gamepad1.y && !robot.isMotorBusy(Lift) && !isLiftUp) {
    robot.setMotorMode(Lift, DcMotor.RunMode.STOP_AND_RESET_ENCODER)

    robot.motors[Lift]?.targetPosition = (CPR * GEAR_RATIO * ROTATIONS).toInt()

    robot.motors[Lift]?.power = -0.99
    robot.motors[Lift]?.mode = DcMotor.RunMode.RUN_TO_POSITION

    isLiftUp = !isLiftUp
}
//===============
```

## Autonomous

### Paths

![Autonomous diagram](./img/3.0.0/auto_diagram.png)

### Moving around during autonomous

We use a mixed tactic: until it scans the navigation target, the robot moves fixed distances by computing encoder ticks and feeding them to the motors; after that it drives for a specified time with a given power using `RUN_USING_ENCODERS` (many thanks to Quantum Robotics for the `RUN_USING_ENCODERS` tip).  

We decided this is the best way to move because:
- moving all the autonomous period by time was greatly imprecise
- moving only by using encoder ticks was prone to errors  

A tactic we want to experiment with in the future is using custom PID controllers (again, thanks to Quantum Robotics for telling us about those things).

### Sampling

The sampling takes some precious time to complete so it needs to be completed as quick as possible. For that we chose to first scan the rightmost mineral first as we need to move to the left in any scenario.   

![Sampling path](./img/3.0.0/corner.png)


### Recognizing the navigation targets

During the regionals there was a moment when our driver forgot to select the correct OpMode for the Autonomous period. Because of that we lost the match. That's why we started thinking if we could write a single autonomous program.  

Scanning the navigation targets from the field walls has proven to be a good solution.  

We took the code for recognizing the targets from the last year's example and adapted it to Rover Ruckus:

```kotlin
enum class DepotPos {
    Left,
    Right
}

fun Robot.recognizeVuMark(): DepotPos {
    val targetsRoverRuckus  = this.vuforia!!.loadTrackablesFromAsset("RoverRuckus")

    // Set names for each navigation target to identify them easily
    val blueRover = targetsRoverRuckus[0]
    blueRover.name = "Blue-Rover"
    val redFootprint = targetsRoverRuckus[1]
    redFootprint.name = "Red-Footprint"
    val frontCraters = targetsRoverRuckus[2]
    frontCraters.name = "Front-Craters"
    val backSpace = targetsRoverRuckus[3]
    backSpace.name = "Back-Space"

    val allTrackables = ArrayList<VuforiaTrackable>()
    allTrackables.addAll(targetsRoverRuckus )

    targetsRoverRuckus.activate()

    var foundTarget: String = "NONE"
    while (foundTarget == "NONE" && opModeIsActive) {
        for (trackable in allTrackables) {
            if ((trackable.listener as VuforiaTrackableDefaultListener).isVisible) {
                telemetry.addData("Visible Target", trackable.name)
                foundTarget = trackable.name
                break
            }
        }

        telemetry.addData("working", "working")
        telemetry.update()
    }

    return when (foundTarget) {
        "Back-Space", "Front-Craters" -> DepotPos.Right
        else -> DepotPos.Left
    }
}
```

The `recognizeVuMark()` extension function returns a `DepotPos` enum that expresses on which side of the target is the Team Depot. 

### What do we do with that information?

After learning on what side of the navigation target the Team Depot is, the program splits in two: when the robot goes to the left or to the right.  

```kotlin
when (depotPos) {
    DepotPos.Left -> {
        // claiming and parking
    }
    DepotPos.Right -> {
        // claiming and parking
    }
```

### Claiming

After the robot goes into the team depot, it lowers the team marker using a Servo.

### Parking

When going into the crater, the robot uses coroutines to raise the intake and to extend it while moving forward.

```kotlin
// Run this code in parallel with whatever was before or whatever comes next 
runParallel {
    val stopwatch = ElapsedTime()
    
    // Raise the intake arm
    robot.setMotorPower(Motors.IntakeRotation, -0.5)
    while (stopwatch.seconds() < 0.89 && opModeIsActive()) { }
    robot.setMotorPower(Motors.IntakeRotation, 0.0)

    // Extend the intake arm
    robot.extendArm()
}

// Move the robot forward for 2.5 seconds
robot.moveForSeconds(2.5, -1.0)
```

___
0. https://semver.org/