---
prev: ch1_general_programming_decisions
next: ch3_gigy_1_0_0_alpha_1
---

# Software Team Goals

## Make it easy to read (and write)

Writing bad code is guaranteed to give you headaches. Learning from our early experience, where trying to find a piece of code takes longer that modifying it, we decided to always strive to write beautiful code. This way, it's easier to read, and easier to build on. 


## Use modern technologies

The tech world is moving fast, so it's hard to keep up with it, but it's worth it. Learning the latest technologies and using them in the context of this competition allows us to build a good understanding around them.

## Help others

Open-source software is taking over in the last few years. We believe that this enables more people than ever to learn programming (or to get better at it). That's why we want to help as many young programmers (older ones are accepted too 😁) by releasing all our code on GitHub.

## Have fun

While we try to put the _pro_ in _programming_, coding without enjoying what you're doing isn't worth it. We try to make the programming experience as fun and pleasing as possible in every little way, from **cool** 😎™ editor color schemes to wireless APK uploading and easy to use abstractions.
