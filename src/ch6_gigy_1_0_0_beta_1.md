---
prev: ch5_gigy_1_0_0_alpha_3
next: ch7_gigy_1_0_0
---

# Gigy 1.0.0-beta.1+20181117.bucharest
*Note: Gigy uses semantic versioning<sup>0</sup> to differentiate himself from his past (or future) selves.*

This was the first actual competition robot we built. For versions newer than 1.0.0-beta.1 and including this one you can find tags with the version name associated with commits on our GitHub repo<sup>1</sup>. 

## <span>TeleOp</span>

The TeleOp was pretty straight forward, since the only thing Gigy could do is move.
We separated the hardware code from the logic by using the `RoverRuckusHardware` class.  
I'll come back to the hardware class after an explanation of the movement logic.  
For the `TeleOp` I chose an Iterative OpMode as in this case it reduces nesting and its functions are pretty intuitive.  
```kotlin
@TeleOp(name = "ROBOT", group = "Rover Ruckus") 
class RoverRuckus : OpMode() {
    // ...
}
```
<!-- ![rover_ruckus_class](img/1.0.0-beta.1/rover_ruckus_class.png) -->

The only field in this class is the hardware representation:
```kotlin
private val hardware = RoverRuckusHardware()
```

<!-- ![hardware_field](img/1.0.0-beta.1/hardware_field.png) -->

The initialization code just takes care of the hardware using the `hardwareMap`:
```kotlin
override fun init() {
    hardware.init(hardwareMap)
}
```
<!-- ![](img/1.0.0-beta.1/hardware_init.png) -->

The code that handles moving the robot is only a few lines long and look like this (the explanation is in the comments):
```kotlin
override fun loop() {
    /** How much does it want to move forward - using Left Trigger */
    val throttle = gamepad1.left_trigger.toDouble()
    /** How much does it want to move backward - using Right Trigger */
    val brake = gamepad1.right_trigger.toDouble()

    /** How much does it want to move left-right - using the right stick's X axis */
    val horizontalMovement = -gamepad1.left_stick_x.toDouble()

    // Motors can receive power with values between -1 and +1
    hardware.leftMotorPower = Range.clip(throttle - brake - horizontalMovement, -1.0, 1.0)
    hardware.rightMotorPower = Range.clip(throttle - brake + horizontalMovement, -1.0, 1.0)

    // Send information to the Drive Station to make them able to know what's happening to the robot

    telemetry.addData("Movement", "Throttle: $throttle | Brake: $brake | Left-Right: $horizontalMovement")
}
```
<!-- ![robot movement](img/1.0.0-beta.1/opmode_loop.png) -->

The hardware class encapsulates all the motors programming. It's a simple Kotlin class that has fields for every motor:
![hardware class](./img/1.0.0-beta.1/hardware_class.png)

Also, for each of them it also has custom power setters and getters, because the motors themselves are private:
![power setters](./img/1.0.0-beta.1/power_setters.png)

In the `init` function of the `RoverRuckusHardware` class I initialize all the motors from a given hardware map:
![hardware init](./img/1.0.0-beta.1/hardware_init_full.png)

## <span>Autonomous</span>

I tried coding the whole autonomous on the competition day, at Bucharest. Me, the engineers and the drivers who were constantly helping got close to finishing it but the design had many flaws and the time was too short. We ended up making the robot recognize minerals and perform the sampling in autonomous.  

The code for recognizing minerals is adapted from the Java Tensorflow Lite tutorial on the GitHub FTC Wiki<sup>2</sup>.  


The adapted code is able to save the gold position when recognizing minerals:
```kotlin
if (opModeIsActive()) {
    /** Activate Tensor Flow Object Detection.  */
    if (tfod != null) {
        tfod!!.activate()
    }

    var goldPos = -1 // Here we store the gold mineral position
    // -1 => not found
    //  0 => left
    //  1 => center
    //  2 => right

    if (tfod != null) {
        // We look at the minerals until we recognize the gold one
        while (goldPos == -1) {
            val updatedRecognitions = tfod!!.updatedRecognitions
            if (updatedRecognitions != null) {
                //...
            }
        }
    }
}
```

The gold mineral position is calculated when the robot recognizes all three minerals:
```kotlin
if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
    if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
        telemetry.addData("Gold Mineral Position", "Left")
        goldPos = 0
    } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
        telemetry.addData("Gold Mineral Position", "Right")
        goldPos = 2
    } else {
        telemetry.addData("Gold Mineral Position", "Center")
        goldPos = 1
    }
}
```

After the mineral is found, the robot moves to perform the sampling according to the gold's position:
```kotlin
when (goldPos) {
    // left
    0 -> {
        rotateLeft()
        moveForward(time=2.5)
    }
    // center
    1 -> {
        moveForward(time=2.5)
    }
    // right
    2 -> {
        rotateRight()
        moveForward(time=2.65)
    }
}
```

The rotation and movement functions are pretty much the same. Here is the code for `rotateLeft`:

```kotlin
private fun rotateLeft(motorPower: Double = 0.25, time: Double = rotationTime) {
    elapsedTime.reset()
    while (elapsedTime.seconds() < time) {
        hardware.leftMotorPower = -motorPower
        hardware.rightMotorPower = motorPower
   }

    resetMotors()
}
```


As you can see, rotation is purely based on time so the duration is found through experiments. The logic for `rotateRight` differs by making the right motor take negative power, instead of the left one and for `moveForward` both motors have positive powers.

___
0. https://semver.org
1. https://github.com/BoogeyBots/robot_code
2. https://github.com/ftctechnh/ftc_app/wiki/Java-Sample-TensorFlow-Object-Detection-Op-Mode
