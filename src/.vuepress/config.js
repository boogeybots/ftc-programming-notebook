module.exports = {
  base: '/ftc-programming-notebook/',
  title: 'Programming Notebook - Rover Ruckus',
  description: 'The programming notebook of team RO141 BoogeyBots for the 2018-2019 Season (Rover Ruckus)',
  themeConfig: {
    sidebar: 'auto',
    displayAllHeaders: true // Default: false
  }
}
